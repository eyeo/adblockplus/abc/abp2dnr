# abp2dnr Development Guide

## Requirements

The system requirements for developing abp2dnr are the same as for using
it. Please start by following the "Requirements" section of
[README.md](https://gitlab.com/eyeo/adblockplus/abc/abp2dnr/-/blob/main/README.md).

## Getting the code

Code is available on [Gitlab](https://gitlab.com/eyeo/adblockplus/abc/abp2dnr).

After checking out, you'll need to use `git submodule` and `npm` to fetch
project dependencies.

```sh
git submodule update --init
npm install
```

## Testing

### Unit Tests

Unit tests live in the `tests/` directory. You can run them by typing this command:

```sh
npm test
```

### Linting

Linting checks for consistent coding style and common coding errors. Linting the
code by running this command:

```sh
npm run lint
```

## Updating RE2

This repo attempts to provide an implementation of `isRegexSupported` which
matches the implementation in Chromium. This means having the same version of
Google's RE2 library, and calling it with the same parameters. This might need
to be updated from time to time to stay up to date with developments in
Chromium and these libraries.

- The current version of RE2 used in Chromium can be found
  [here](https://source.chromium.org/chromium/chromium/src/+/main:third_party/re2/README.chromium).
- RE2 currently depends on Abseil. The current version of Abseil used in
  Chromium can be found
  [here](https://source.chromium.org/chromium/chromium/src/+/main:third_party/abseil-cpp/README.chromium).
- Chromium's RE2 options for DNR rules can be found
  [here](https://source.chromium.org/chromium/chromium/src/+/master:extensions/browser/api/declarative_net_request/utils.cc;l=303-324).
- If these instructions for updating RE2 and its dependencies are out of date,
  [Node RE2](https://github.com/uhop/node-re2/) has similar concerns around
  building RE2.

### Updating the RE2 submodule to a new commit

These are the steps to update the version of RE2 we're building to a new git
commit.

- Go into the submodule directory: `cd re2`
- Checkout the version you want to use: `git checkout <commit-hash>`
- Go back to the abp2dnr directory: `cd ..`
- Stage the new version using git: `git add re2`
- Open binding.gyp. Check that the `"sources"` section includes the RE2 `.cc`
  files listed in `re2/CMakeLists.txt`, under the `RE2_SOURCES` variable.

### Updating Abseil to a new commit

These are the steps to update the version of Abseil we're building to a new git
commit. We only use Abseil because it is a dependency of RE2.

- Go into the submodule directory: `cd abseil-cpp`
- Checkout the version you want to use: `git checkout <commit-hash>`
- Go back to the abp2dnr directory: `cd ..`
- Stage the new version using git: `git add abseil-cpp`
- Open binding.gyp. I'm not sure how to figure out which files should be in the
  `"sources"` section for Abseil, update this doc if you know! I used the Abseil
  sources listed from [Node RE2](https://github.com/uhop/node-re2/), and then
  additionally added the file that the Windows CI complained about.

### Checking the RE2 options we use

We call RE2 in [is_regex_supported.cc](./lib/is_regex_supported.cc).
