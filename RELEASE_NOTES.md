Unreleased
==========

🐫 1.1.1 - 2024-08-12 🐫
=======================

## Changes

- Update webext-ad-filtering-solution version to 1.11.0, this specifically means:
  - Use the requestDomains condition in DNR rules for main frames, so that $document
  filters work correctly together with the $domain filter option. (EXT-55)

🍓 1.1.0 - 2024-02-08 🍓
=======================

## Changes

- Update webext-ad-filtering-solution version to 1.4.1, this specifically means:
  - URL filters with wildcards (.*) are no longer converted into DNR rules
    as Chromium does not support it. (EE-274)

🎄 1.0.0 - 2023-12-05 🎄
========================

## Breaking Changes

- `regexCheck` has been removed. Please use `isRegexSupported`.
- For use in scripts, it's now recommended to import the root of the package,
  and not the `lib/abp2dnr.js` script directly. The example in the readme file
  has been updated accordingly.

## New features

- API docs are now available at <https://eyeo.gitlab.io/adblockplus/abc/abp2dnr/>.
- For command line use, it's now possible to run abp2dnr with `npx
  @eyeo/abp2dnr` globally, or `npx abp2dnr` in a npm project that depends on
  abp2dnr.

## Changes

- Updated RE2 to the version currently used on Chromium's main branch. This may
  have minor effects on the exact complexity limits of valid regex filters.
- Filter texts are now normalized internally when calling `convertFilter`, so
  there is no need to call `Filter.normalize` from `adblockpluscore` anymore.

🎅 0.3.0 - 2023-11-29 🎅
========================

- Bump the version of Node supported. We now require Node 18 or higher. We test
  on both Node 18 and Node 20.
- Update the DNR rule generation to use the webext-ad-filtering-solution
  package, v1.2.0.
  - The `isUrlFilterCaseSensitive` property is now always set. This previously
    relied the default value if the filter was case sensitive, but the default
    value was changed in Chromium 118 leading to inconsistent behaviour. This
    affected filter texts that use the `match-case` option.

0.2.0 - 2022-10-14
==================

## Breaking Changes

- `convertFilter` now accepts a filter text as a string, rather than a filter
  object.
- `convertFilter` no longer accepts the `isRegexSupported` parameter.
- The rule priority number constants `GENERIC_PRIORITY`,
  `GENERIC_ALLOW_ALL_PRIORITY`, `SPECIFIC_PRIORITY` and
  `SPECIFIC_ALLOW_ALL_PRIORITY` are no longer exposed.

## New Features

- New function `isRegexSupported` and `regexCheck` have been added. These can be
  used to check if a regex can be used in a DNR rule.

## Internal

- The rule generating code has all been migrated into the "adblockpluscore"
  package, which this package depends on.

0.1.0 - 2021/02/18
==================

- Initial release
