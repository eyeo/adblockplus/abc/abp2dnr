/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/** @module abp2dnr */

"use strict";

const {isRegexSupported} = require("../build/Release/isRegexSupported");
const {createConverter} = require("@eyeo/webext-ad-filtering-solution/adblockpluscore/lib/dnr/index.js");
const {FilterParsingError} = require("@eyeo/webext-ad-filtering-solution/adblockpluscore/lib/filters/index.js");
const {Filter} = require("@eyeo/webext-ad-filtering-solution/adblockpluscore/lib/filterClasses.js");

function regexCheck(r) {
  let result = isRegexSupported(r);
  return result.isSupported;
}

let converter = createConverter({isRegexSupported: regexCheck});

/**
 * @typedef RegexOptions
 * See {@link https://developer.chrome.com/docs/extensions/reference/declarativeNetRequest/#type-RegexOptions}
 * @type {object}
 * @property {string} regex The Regular Expression string.
 * @property {boolean?} isCaseSensitive Whether the Regex is case sensitive.
 * @property {boolean?} requireCapturing Whether the Regex require capturing.
 */

/** @typedef IsRegexSupportedResult
 * See {@link https://developer.chrome.com/docs/extensions/reference/declarativeNetRequest/#type-IsRegexSupportedResult}
 * @type {object}
 * @property {boolean} isSupported True if the regex can be used in a DNR rule.
 * @property {string?} reason The reason if isSupported is false.
 */

/** Returns if a regex is supported by DNR. This function is a reimplementation
 * of the Chrome API, adapted to run in Node. {@link https://developer.chrome.com/docs/extensions/reference/declarativeNetRequest/#method-isRegexSupported}
 * @param {RegexOptions} regexOptions The regular expression description.
 * @returns {IsRegexSupportedResult}
 */
exports.isRegexSupported = function(regexOptions) {
  return isRegexSupported(regexOptions);
};

/**
 * Converts a filter into DNR rules. It will call `isRegexSupported` if
 * needed. A single filter could result in multiple DNR rules.
 *
 * @async
 * @param {string} filter The filter text.
 * @return {Array<object>} An array of DNR rules.
 * @throws {FilterParsingError} Thrown if the filter can't be converted into DNR
 * rules. This could be because the filter is not a request blocking filter, or
 * because it uses a feature like sitekeys or complex regex filters which DNR
 * does not support.
 */
exports.convertFilter = async function convertFilter(filter) {
  let normalized = Filter.normalize(filter);
  let result = await converter(normalized);
  if (result instanceof FilterParsingError) {
    throw result;
  }
  return result;
};
