/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const expect = require("expect");

const {isRegexSupported} = require("../lib/converter.js");

describe("isRegexSupported", function() {
  it("should throw if arguments are invalid", async function() {
    expect(() => isRegexSupported()).toThrow();
    expect(() => isRegexSupported(1)).toThrow();
    expect(() => isRegexSupported({})).toThrow();
    expect(() => isRegexSupported({foo: "bar"})).toThrow();
  });

  it("should accept a valid regular expression", async function() {
    expect(isRegexSupported({
      regex: "[0-9]+"
    })).toEqual({isSupported: true});
  });

  it("should accept a valid regular expression with options", async function() {
    expect(isRegexSupported({
      regex: "[0-9]+",
      isCaseSensitive: false,
      requireCapturing: true
    })).toEqual({isSupported: true});
  });

  it("should reject an invalid regular expression", async function() {
    expect(isRegexSupported({
      regex: "[a-9]+"
    })).toEqual({isSupported: false, reason: "syntaxError"});
  });

  it("should reject an invalid regular expression with options", async function() {
    expect(isRegexSupported({
      regex: "[a-9]+",
      isCaseSensitive: false,
      requireCapturing: true
    })).toEqual({isSupported: false, reason: "syntaxError"});
  });

  it("should reject long regular expression", async function() {
    expect(isRegexSupported({
      regex: "[0-9]+".repeat(1000)
    })).toEqual({isSupported: false, reason: "memoryLimitExceeded"});
  });

  it("should reject long regular expression with options", async function() {
    expect(isRegexSupported({
      regex: "(a)".repeat(50)
    })).toEqual({isSupported: true});

    expect(isRegexSupported({
      regex: "(a)".repeat(50),
      requireCapturing: false
    })).toEqual({isSupported: true});

    expect(isRegexSupported({
      regex: "(a)".repeat(50),
      requireCapturing: true
    })).toEqual({isSupported: false, reason: "memoryLimitExceeded"});
  });
});
